#include "interface.h"
#include "listes.h"
#include <gtk/gtk.h>
#include "gtk-gui/gui.h"
#include "gtk-gui/canvas.h"
#include "gtk-gui/gui-top.h"
#include "gtk-gui/gui.c"
#include "gtk-gui/gui-top.c"
#include "gtk-gui/canvas.c"

static Tgui*			gui;
static Tsegm			line;

void print_game(point **plateau, int n) {
	gui_redraw(gui);
}

/*action {JOUER_COUP, ANNULER, REJOUER, HELP, SAVE} action*/
action choisir_action(){
	cvs_setModeStd(gui->canvas);
	gui->action = GUI_ACT_Undef;
	gui->actionSegm.p1 = int2_init(0,0);
    gui->actionSegm.p2 = int2_init(0,0);
    gtk_main();
	line = gui->actionSegm;
	if (gui->action == GUI_ACT_Segment) return JOUER_COUP;
	if (gui->action == GUI_ACT_Undo) return ANNULER;
	if (gui->action == GUI_ACT_Redo) return REJOUER;
	if (gui->action == GUI_ACT_Help) return HELP;
	if (gui->action == GUI_ACT_Quit) { gui_close(gui); exit(0); }
	/*if (gui->action == GUI_ACT_TODO) return SAVE;*/
	else return HELP;
}

void open_gui(point **plateau, int n) {
	gui = gui_open(300,20);

	int i=0, j=0;
	int nbPoints = 0;
	Tint2 tmp;
	int count = 0;
	Tint2 *pointsPlateau;

	for (i=0 ; i<n ; i++) {
		for (j=0 ; j<n ; j++) {
			if (plateau[i][j].flag) nbPoints++;
		}
	}

	pointsPlateau = (Tint2*)malloc(nbPoints*sizeof(Tint2));
	for (i=0 ; i<n ; i++) {
		for (j = 0; j < n; j++) {
			if (plateau[i][j].flag) {
				tmp.x = i;
				tmp.y = j;
				pointsPlateau[count] = tmp;
				count++;
			}
		}
	}
	gui_addPoints(gui,pointsPlateau,nbPoints);
	gui_free(pointsPlateau);
}

enum action saisir_coup(list_couple coups_possibles, couple* coupChoisi, ligne* ligneChoisie, point** plateau, int n){
	enum action action = choisir_action();

	if(action == JOUER_COUP) {
		if ( abs(line.p1.x-line.p2.x)>9||
            abs(line.p1.y-line.p2.y)>9 )
            gui_error(gui, "le segment (%d,%d) --> (%d,%d) est invalide",
                line.p1.x,line.p1.x, line.p2.y,line.p2.y);
        else
            gui_addLines(gui,&line,1);
		/* *coupChoisi = coord; */
	}

	if(action == REJOUER) {
		gui_addLines(gui,&line,1);
	}

	if(action == ANNULER) {
		gui_supLastLine(gui);
	}

	if(action == HELP) {
			list_ligne lignesPossibles;
			list_couple		tmpcoupsPossibles = coups_possibles;
			int nbPoints = len_couple(coups_possibles);
			Tsegm *lignesHelp = (Tsegm*)malloc(nbPoints*8*sizeof(Tsegm));
			int count = 0;
			Tint2 tmp;
			couple 			realCoupChoisi;
			Tsegm lastLine;
			int flag = 0;
			list_ligne 		listeCoups = (list_ligne)empty();
			int nombreLignes = 0;

			/*pour chaque couple possible on va calculer les lignes possibles*/
			while (coups_possibles!=NULL) {
				coupChoisi->x=coups_possibles->val.x;
				coupChoisi->y=coups_possibles->val.y;
				lignesPossibles = list_lignes_possibles(plateau,n,*coupChoisi);
				/*on parcourt les lignes possibles pour les ajouter au tableau*/
				while (lignesPossibles!=NULL) {
					tmp.x = lignesPossibles->val.tab_pts[0].x;
					tmp.y = lignesPossibles->val.tab_pts[0].y;
					lignesHelp[count].p1 = tmp;
					tmp.x = lignesPossibles->val.tab_pts[4].x;
					tmp.y = lignesPossibles->val.tab_pts[4].y;
					lignesHelp[count].p2 = tmp;
					count++;
					lignesPossibles = lignesPossibles->next;
				}
				coups_possibles = coups_possibles->next;
			}

              if ( gui_getSegOfSet(gui,lignesHelp, count, &line)==GUI_ACT_Quit) {
                  gui_close(gui);
					return 0;  
				}
			  lastLine = line;
			  while (tmpcoupsPossibles!=NULL) {
  				coupChoisi->x=tmpcoupsPossibles->val.x;
  				coupChoisi->y=tmpcoupsPossibles->val.y;
  				lignesPossibles = list_lignes_possibles(plateau,n,*coupChoisi);
  				/*on parcourt les lignes possibles pour trouver la ligne correspondant
				à celle de l'utilisateur*/
  				while (lignesPossibles!=NULL) {
  					if (lignesPossibles->val.tab_pts[0].x == lastLine.p1.x &&
  						lignesPossibles->val.tab_pts[0].y == lastLine.p1.y &&
  						lignesPossibles->val.tab_pts[4].x == lastLine.p2.x &&
  						lignesPossibles->val.tab_pts[4].y == lastLine.p2.y) {
							*ligneChoisie = lignesPossibles->val;
							flag = 1;
							printf("ligne trouvée\n");
							printf("user: %i, %i, %i, %i\n",lastLine.p1.x,lastLine.p1.y,lastLine.p2.x,lastLine.p2.y);
							printf("\n");
							break;
						}
  					lignesPossibles = lignesPossibles->next;
  				}
				if(flag) break;
  				tmpcoupsPossibles = tmpcoupsPossibles->next;
  			}
			if(!flag) gui_error(gui, "le segment (%d,%d) --> (%d,%d) est invalide",
				  line.p1.x,line.p1.x, line.p2.y,line.p2.y);
			realCoupChoisi = ligneChoisie->createur;
			  play_coup(plateau,realCoupChoisi,*ligneChoisie,&listeCoups,&nombreLignes);

              gui_addLines(gui,&line,1);
	}

	return action;
}

ligne saisir_ligne(list_ligne lignes_possibles) {
	/*UNIQUEMENT POUR LE MODE CONSOLE*/
	ligne l;
	return l;
}

void definir_nomFichier(char *buff) {
	printf("Saisissez le nom du fichier à exporter\n");
	scanf("%s",buff);
}

void file_exception() {
	printf("Le chargement/sauvegarde de votre plateau a échoué. Vérifiez votre fichier ou votre nom de fichier.\n");
}

void afficher_score(int *nombreLignes){
	printf("score :%i\n",*nombreLignes);

}

int definir_TaillePlateau(int *n){
  char c[100];
  int m;
  printf("Saisissez la taille du plateau :\n");
  scanf("%s",c);
  m=strtol(c,NULL,10);
  if (m<=0)return 0;
  else{
    *n=m;
    return 1;
  }

}

void help(){
  printf("Pour lancer l'exécutable, vous pouvez soit mettre aucun argument, soit donner un argument qui est le nom du fichier source (contenant le plateau éventuellement sauvegardé)\n");

}

void commande_inconnue(){
  printf("La commande saisie est inconnue\n");
}
