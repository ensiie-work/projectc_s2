#include "interface.h"
#include "listes.h"

Tgui *init() {
	return gui_open(300,20);
}

void print_game(point **plateau, int n) {

}

couple saisir_coup(list_couple coups_possibles){
  couple coord;
  char c;

  printf("Voulez-vous afficher la liste des coups possibles ? (Y/N)\n");
  scanf("%1s",&c);
  if (c=='Y' || c=='y'){
    printf("Les coups possibles sont :\n");
    while (coups_possibles != NULL){
      printf("%i,%i ",coups_possibles->val.x+1, coups_possibles->val.y+1);
      coups_possibles=coups_possibles->next;
    }
  }

  printf("\nAfficher l'aide? (Y/N)\n");
  scanf("%1s",&c);
  if (c=='Y' || c=='y') help();
  printf("Saisir le coup :\n");
  scanf("%i",&coord.x);
  scanf("%i",&coord.y);
  coord.x -= 1;
  coord.y -= 1;
  return coord;
}

ligne saisir_ligne(list_ligne lignes_possibles) {
	char const* nom_type_ligne[] = {"L horizontale", "L verticale", "L diagonale 1", "L diagonale 2"};
	char ch;
	ligne l;
	int k;
	list_ligne start = lignes_possibles;
	couple err;

	err.x = -1;
	err.y = -1;

	int type_ligne_int;

	printf("Les lignes possibles sont :\n");
	while (lignes_possibles != NULL){
		printf("%s\n", nom_type_ligne[lignes_possibles->val.type]);
		lignes_possibles = lignes_possibles->next;
	}

	printf("Saisir le type de ligne souhaité:\n");
	printf("\t1: horizontale\n\t2: verticale\n\t3: diagonale /\n\t4: diagonale \\\n");
	scanf("%1s",&ch);
	type_ligne_int = strtol(&ch,NULL,10);
	while (type_ligne_int<=0 || type_ligne_int>4) {
		printf("Veuillez entrer une ligne valide :\n");
		printf("\t1: horizontale\n\t2: verticale\n\t3: diagonale /\n\t4: diagonale \\\n");
		fflush(stdin);
		scanf("%1s",&ch);
		type_ligne_int = strtol(&ch,NULL,10);
	}
	type_ligne_int--;
	l.type = (type_ligne)type_ligne_int;

	/* IMPORTANT */
	lignes_possibles = start;
	int ligne_found = 0;
	while (lignes_possibles != NULL && !ligne_found){
		if(lignes_possibles->val.type == l.type) {
			l.createur = (lignes_possibles->val).createur;
			for (k = 0 ; k<5 ; k=k+1) {
				l.tab_pts[k] = (lignes_possibles->val).tab_pts[k];
			}
			l.type = (lignes_possibles->val).type;
			ligne_found = 1;
		}
		lignes_possibles = lignes_possibles->next;
	}
	/* si le type de ligne souhaité n'est pas disponible */
	if (ligne_found==0) {
		l.createur = err;
		return l;
	}
	return l;
}

void definir_nomFichier(char *buff) {
	printf("Saisissez le nom du fichier à exporter\n");
	scanf("%s",buff);
}

void file_exception() {
	printf("Le chargement/sauvegarde de votre plateau a échoué. Vérifiez votre fichier ou votre nom de fichier.\n");
}

void afficher_score(int *nombreLignes){
	printf("score :%i\n",*nombreLignes);

}

int definir_TaillePlateau(int *n){
  char c[100];
  int m;
  printf("Saisissez la taille du plateau :\n");
  scanf("%s",c);
  m=strtol(c,NULL,10);
  if (m<=0)return 0;
  else{
    *n=m;
    return 1;
  }

}

void help(){
  printf("Pour lancer l'exécutable, vous pouvez soit mettre aucun argument, soit donner un argument qui est le nom du fichier source (contenant le plateau éventuellement sauvegardé)\n");

}

void commande_inconnue(){
  printf("La commande saisie est inconnue\n");
}
