CC = -g -Wall -Wextra -ansi
CIBLE = main
OBJ = main.o plateau.o historique.o interface_$(INTERFACE).o listes.o
GTK1 = `pkg-config --cflags gtk+-3.0`
GTK2 = `pkg-config --libs gtk+-3.0` -lm
INTERFACE = console

all : main

main : main.o interface_$(INTERFACE).o historique.o plateau.o listes.o
	gcc $(CC) $(GTK1) $(OBJ) $(GTK2) -o $(CIBLE)

main.o : main.c interface.h historique.h plateau.h listes.h
	gcc $(CC) $(GTK1) -c main.c $(GTK2)

interface_$(INTERFACE).o : interface_$(INTERFACE).c interface.h plateau.h
	gcc $(CC) $(GTK1) -c interface_$(INTERFACE).c $(GTK2)

historique.o : historique.c historique.h plateau.h listes.h
	gcc $(CC) $(GTK1) -c historique.c $(GTK2)

plateau.o : plateau.c plateau.h listes.h interface.h
	gcc $(CC) $(GTK1) -c plateau.c $(GTK2)

listes.o : listes.c listes.h plateau.h
	gcc $(CC) $(GTK1) -c listes.c $(GTK2)

clean :
	rm $(OBJ) $(CIBLE)

re : clean all
