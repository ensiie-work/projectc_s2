#include "historique.h"
#include "plateau.h"
#include "interface.h"
#include "listes.h"
#include <string.h>
#include <stdlib.h>

int main(int ac, char **av) {
	point 			**plateau;
	int				n 	= 0;
	int				nombreLignes = 0;
	int 			coupPossible = 1;
	list_ligne 		listeCoups = (list_ligne)empty();
	list_ligne		listeCoupsAnnules = (list_ligne) empty();
	couple			coupChoisi;
	ligne			ligneChoisie;
	list_ligne		lignesPossibles =  (list_ligne)empty();
	list_couple		coupsPossibles =  (list_couple)empty();
	char 			buff[1024];

	if (ac > 3) {
		help();
		return 1;
	}
	/*else if (ac == 3 && strcmp( av[1], "-r") != 0) {
		help();
		return 1;
	}
	else if (ac == 2 && strcmp( av[1], "-h") != 0) {
		help();
		return 1;
	}
	else if (ac == 2) {
		help();
		return 0;
	}*/
	else if (ac == 3) {
		plateau = file_to_plateau(av[2]);

		if(plateau == NULL) {
			file_exception();
			exit(-1);
		}

		n = file_plateau_size(av[2]);
	} else {
		while(!definir_TaillePlateau(&n));
		plateau = random_plateau(n);
	}

	open_gui(plateau,n);

	/* Boucle principale */
	while(coupPossible) {
			print_game(plateau, n);
			coupsPossibles = list_coups_valides(plateau,n);

			switch(saisir_coup(coupsPossibles,&coupChoisi,&ligneChoisie,plateau,n)) {
				case JOUER_COUP:
					lignesPossibles = list_lignes_possibles(plateau,n,coupChoisi);

					if(contains_couple(coupChoisi,coupsPossibles)) {
						ligneChoisie = saisir_ligne(lignesPossibles);
					}

					if(contains_ligne(ligneChoisie,lignesPossibles)) {
						play_coup(plateau,coupChoisi,ligneChoisie,&listeCoups,&nombreLignes);
					}

					break;
				case ANNULER:
					annuler(plateau, &listeCoups, &listeCoupsAnnules, &nombreLignes);
					break;
				case REJOUER:
					if (listeCoupsAnnules != NULL) {
						ligneChoisie = rejouer_coup(plateau, &listeCoups, &listeCoupsAnnules, &nombreLignes);
					}
					break;
				case HELP:
					help();
					break;
				case SAVE:
					definir_nomFichier(buff);

					if(plateau_to_file(plateau,buff,n) == -1)
						file_exception();

					break;
				default:
					commande_inconnue();
					break;
			}

		afficher_score(&nombreLignes);
		coupPossible = !is_over(plateau,n);
	}

	/* La partie est terminée */
	afficher_score(&nombreLignes);

	return 0;
}
