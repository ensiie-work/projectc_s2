#include "plateau.h"
#include "interface.h"
#include "listes.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

point **random_plateau(int n) {
	point	**plateau;
	int 	i;
	int 	j;

	plateau = (point**)malloc(n*sizeof(point*));
	for (i=0 ; i<n ; i++) {
		plateau[i] = (point*)malloc(n*sizeof(point));
	}
	for (i=0; i<n ; i++) {
		for (j=0 ; j<n ; j++) {
			plateau[i][j].flag = rand() % 2;
			plateau[i][j].liste_type = (list_type_ligne) empty();
		}
	}
	return plateau;
}

void play_coup(point **plateau, couple c, ligne l, list_ligne *listeCoups, int *nombreLignes) {
	plateau[c.x][c.y].flag = 1;
	/*list_type_ligne tmp;*/
	int i;

	for(i=0; i < 5; i++)  {
	/*	printf("%i, %i\n", l.tab_pts[i].x,l.tab_pts[i].y); */
	/*	tmp = (list_type_ligne)plateau[l.tab_pts[i].x][l.tab_pts[i].y].liste_type;*/
		add_type_ligne(l.type,&plateau[l.tab_pts[i].x][l.tab_pts[i].y].liste_type);
	}

	add_ligne(l, listeCoups);

	(*nombreLignes)++;
}

int get_east(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int east;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (j+k<n && k<5) {
		if (!plateau[i][j+k].flag) break;
		if (contains(LH, plateau[i][j+k].liste_type)){
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	east = k-1;
	return east;
}

int get_west(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int west;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (j-k>=0 && k<5) {
		if (!plateau[i][j-k].flag) break;
		if (contains(LH, plateau[i][j-k].liste_type)){
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	west = k-1;
	return west;
}

int get_north(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int north;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (i-k>=0 && k<5) {
		if (!plateau[i-k][j].flag) break;
		if (contains(LV, plateau[i-k][j].liste_type)) {
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	north = k-1;
	return north;
}

int get_south(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int south;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (i+k<n && k<5) {
		if (!plateau[i+k][j].flag) break;
		if (contains(LV, plateau[i+k][j].liste_type)) {
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	south = k-1;
	return south;
}

int get_se(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int se;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (i+k<n && j+k<n && k<5) {
		if (!plateau[i+k][j+k].flag) break;
		if (contains(D1, plateau[i+k][j+k].liste_type)) {
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	se = k-1;
	return se;
}

int get_sw(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int sw;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (i+k<n && j-k>=0 && k<5) {
		if (!plateau[i+k][j-k].flag) break;
		if (contains(D2, plateau[i+k][j-k].liste_type)) {
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	sw = k-1;
	return sw;
}

int get_ne(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int ne;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (i-k>=0 && j+k<n && k<5) {
		if (!plateau[i-k][j+k].flag) break;
		if (contains(D2, plateau[i-k][j+k].liste_type)) {
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	ne = k-1;
	return ne;
}

int get_nw(point **plateau, couple c, int n) {
	int i, j, k;
	int consec;
	int nw;

	k = 1;
	i = c.x;
	j = c.y;
	consec = 0;
	while (i-k>=0 && j-k>=0 && k<5) {
		if (!plateau[i-k][j-k].flag) break;
		if (contains(D1, plateau[i-k][j-k].liste_type)) {
			if (consec) break;
			consec = 1;
		}
		k++;
	}
	nw = k-1;
	return nw;
}

list_couple list_coups_valides(point **plateau, int n) {
	list_couple res;
	couple 	c;
	int 	i, j, k;
	int 	vois_h = 0, vois_v = 0, vois_d1 = 0, vois_d2 = 0;
	int 	north, south, east, west, ne, nw, se, sw = 0;
	int 	consec = 0;

	res = (list_couple) empty();
	for (i=0 ; i<n ; i++) {
		for (j=0 ; j<n ; j++) {
			if (plateau[i][j].flag) continue;

			for (k=1 ; k<5 ; k++) {
				if (j+k < n && plateau[i][j+k].flag) vois_h++;
				if (j-k >= 0 && plateau[i][j-k].flag) vois_h++;
				if (i+k < n && plateau[i+k][j].flag) vois_v++;
				if (i-k >= 0 && plateau[i-k][j].flag) vois_v++;
				if (i+k<n && j+k<n && plateau[i+k][j+k].flag) vois_d2++;
				if (i-k>=0 && j-k>=0 && plateau[i-k][j-k].flag) vois_d2++;
				if (i+k<n && j-k>=0 && plateau[i+k][j-k].flag) vois_d1++;
				if (i-k>=0 && j+k<n && plateau[i-k][j+k].flag) vois_d1++;
			}

			if (vois_h >= 4) {
				/* jusqu'où on peut aller vers la droite */
				k = 1;
				consec = 0;
				while (j+k<n && k<5) {
					if (!plateau[i][j+k].flag) break;
					if (contains(LH, plateau[i][j+k].liste_type)){
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				east = k-1;

				/* jusqu'où on peut aller vers la gauche */
				k = 1;
				consec = 0;
				while (j-k>=0 && k<5) {
					if (!plateau[i][j-k].flag) break;
					if (contains(LH, plateau[i][j-k].liste_type)){
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				west = k-1;

				/* vérification qu'une ligne est faisable */
				if (west+east>=4) {
					c.x = i;
					c.y = j;
					add_couple(c,&res);
					/* le coup (i,j) est possible, pas la peine de vérifier
					pour d'autres lignes (verticales, diagonales...) */
					continue;
				}

			}

			if (vois_v >= 4) {
				k = 1;
				consec = 0;
				/* jusqu'où on peut aller vers le bas */
				while (i+k<n && k<5) {
					if (!plateau[i+k][j].flag) break;
					if (contains(LV, plateau[i+k][j].liste_type)) {
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				south = k-1;

				/* jusqu'où on peut aller vers le haut */
				k = 1;
				consec = 0;
				while (i-k>=0 && k<5) {
					if (!plateau[i-k][j].flag) break;
					if (contains(LV, plateau[i-k][j].liste_type)) {
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				north = k-1;

				/* vérification qu'une ligne verticale est possible */
				if (north+south >= 4) {
					c.x = i;
					c.y = j;
					add_couple(c,&res);
					/* pas la peine de vérifier d'autres lignes */
					continue;
				}

			}

			if (vois_d2 >= 4) {
				k = 1;
				consec = 0;
				/* jusqu'où on peut aller vers en haut à gauche */
				while (i-k>=0 && j-k>=0 && k<5) {
					if (!plateau[i-k][j-k].flag) break;
					if (contains(D2, plateau[i-k][j-k].liste_type)) {
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				nw = k-1;

				/* jusqu'où on peut aller vers en bas à droite */
				k = 1;
				consec = 0;
				while (i+k<n && j+k<n && k<5) {
					if (!plateau[i+k][j+k].flag) break;
					if (contains(D2, plateau[i+k][j+k].liste_type)) {
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				se = k-1;

				/* vérification qu'une ligne '\' est faisable */
				if (nw+se >= 4) {
					c.x = i;
					c.y = j;
					add_couple(c,&res);
					/* pas la peine de vérifier d'autres lignes */
					continue;
				}
			}

			if (vois_d1 >= 4) {
				k = 1;
				consec = 0;
				/* jusqu'où on peut aller vers en haut à droite */
				while (i-k>=0 && j+k<n && k<5) {
					if (!plateau[i-k][j+k].flag) break;
					if (contains(D1, plateau[i-k][j+k].liste_type)) {
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				ne = k-1;

				/* jusqu'où on peut aller vers en bas à gauche */
				k = 1;
				consec = 0;
				while (i+k<n && j-k>=0 && k<5) {
					if (!plateau[i+k][j-k].flag) break;
					if (contains(D1, plateau[i+k][j-k].liste_type)) {
						if (consec) break;
						consec = 1;
					}
					k++;
				}
				sw = k-1;

				/* vérification qu'une ligne '/' est faisabable */
				if (ne+sw >= 4) {
					c.x = i;
					c.y = j;
					add_couple(c,&res);
					/* pas la peine de vérifier d'autres lignes */
					continue;
				}
			}
		}
	}
	return res;

}

list_ligne list_lignes_possibles(point **plateau, int n, couple c) {
	list_ligne res = (list_ligne) empty();
	ligne 	tmp_l;
	couple 	tmp_c;
	int 	i, j, k;
	int 	ind;
	int 	vois_h = 0, vois_v = 0, vois_d1 = 0, vois_d2 = 0;
	int 	north, south, east, west, ne, nw, se, sw = 0;

	i = c.x;
	j = c.y;

	for (k=1 ; k<5 ; k++) {
		if (j+k < n && plateau[i][j+k].flag) vois_h++;
		if (j-k >= 0 && plateau[i][j-k].flag) vois_h++;
		if (i+k < n && plateau[i+k][j].flag) vois_v++;
		if (i-k >= 0 && plateau[i-k][j].flag) vois_v++;
		if (i+k<n && j+k<n && plateau[i+k][j+k].flag) vois_d2++;
		if (i-k>=0 && j-k>=0 && plateau[i-k][j-k].flag) vois_d2++;
		if (i+k<n && j-k>=0 && plateau[i+k][j-k].flag) vois_d1++;
		if (i-k>=0 && j+k<n && plateau[i-k][j+k].flag) vois_d1++;
	}

	/* pour les lignes horizontales */
	if (vois_h >= 4) {
		/* jusqu'où on peut aller vers la droite */
		east = get_east(plateau, c, n);

		/* jusqu'où on peut aller vers la gauche */
		west = get_west(plateau, c, n);

		/* si une ligne est possible */
		if (west+east>=4) {
			for (k=0 ; k <= west+east+1-5 ; k++ ){
				tmp_l.type = LH;
				tmp_l.createur = c;
				tmp_c.x = i;
				for (ind=0 ; ind < 5 ; ind++) {
					tmp_c.y = j-west+ind+k;
					tmp_l.tab_pts[ind] = tmp_c;				}
				/* ajout de la ligne au résultat */
				add_ligne(tmp_l, &res);
			}
		}
	}

	/* pour les lignes verticales */
	if (vois_v >= 4) {
		/* jusqu'où n=on peut aller vers le haut */
		north = get_north(plateau, c, n);

		/* jusqu'où on peut aller vers le bas */
		south = get_south(plateau, c, n);

		/* si une ligne est possible */
		if (south+north>=4) {
			for (k=0 ; k <= south+north+1-5 ; k++ ){
				tmp_l.type = LV;
				tmp_l.createur = c;
				tmp_c.y = j;
				for (ind=0 ; ind < 5 ; ind++) {
					tmp_c.x = i-north+ind+k;
					tmp_l.tab_pts[ind] = tmp_c;
				}
				/* ajout de la ligne au résultat */
				add_ligne(tmp_l, &res);
			}
		}
	}

	/* pour les lignes diagonales D1 */
	if (vois_d1 >= 4) {
		/* vers en haut à droite */
		ne = get_ne(plateau, c, n);

		/* vers en bas à gauche */
		sw = get_sw(plateau, c, n);

		/* si une ligne est possible */
		if (ne+sw >= 4) {
			for (k=0 ; k <= ne+sw+1-5 ; k++ ){
				tmp_l.type = D1;
				tmp_l.createur = c;
				for (ind=0 ; ind < 5 ; ind++) {
					tmp_c.x = i+sw-ind-k;
					tmp_c.y = j-sw+ind+k;
					tmp_l.tab_pts[ind] = tmp_c;
				}
				/* ajout de la ligne au résultat */
				add_ligne(tmp_l, &res);
			}
		}
	}

	/* pour les lignes diagonales D2 */
	if (vois_d2>=4) {
		/* vers en hau à gauche */
		nw = get_nw(plateau, c, n);

		/* vers en bas à droite */
		se = get_se(plateau, c, n);

		/* si une ligne est possible */
		if (nw+se >= 4) {
			for (k=0 ; k <= nw+se+1-5 ; k++ ){
				tmp_l.type = D2;
				tmp_l.createur = c;
				for (ind=0 ; ind < 5 ; ind++) {
					tmp_c.x = i-nw+ind+k;
					tmp_c.y = j-nw+ind+k;
					tmp_l.tab_pts[ind] = tmp_c;
				}
				/* ajout de la ligne au résultat */
				add_ligne(tmp_l, &res);
			}
		}
	}
	return res;
}

int plateau_to_file(point **plateau, char *dest, int n){
  FILE* fichier = NULL;

  fichier = fopen(dest, "w+");

    if (fichier != NULL){
		fprintf(fichier,"%i;",n);
		int i=0;
		int j=0;
		for (i=0; i<n;i++){
			for (j=0 ; j<n ; j++) {
				list_type_ligne res = plateau[i][j].liste_type;
				fprintf(fichier,"%i",plateau[i][j].flag);
				if (plateau[i][j].flag) {
					while (res!=NULL){
						fprintf(fichier,"%d",res->type);
						res=res->next;
					}
	  			}
				fprintf(fichier,";");
			}
		}
		fclose(fichier);
		return 0;
	} else {
		return -1;
    }
}

int file_plateau_size(char *path){
	FILE *file;
	int c;

	file = fopen(path, "r");

	if (file != NULL) {
		int cptChar=0;
		char buff[10];

		while((c = fgetc(file)) != EOF && c != ';') {
			buff[cptChar] = c;
			cptChar++;
		}
		buff[cptChar+1] = '\0';

		fclose(file);
		return atoi(buff);/*n est la taille du tableau*/
	} else {
		return 0;
	}
}

point** file_to_plateau(char *path){
	FILE *file;
	int c;

	file = fopen(path, "r");

	if (file != NULL) {
		point **plateau;
		int i;
		int j;
		int cptChar=0;
		char buff[1024];
		type_ligne lh= LH;
		type_ligne lv= LV;
		type_ligne d1= D1;
		type_ligne d2= D2;

		while((c = fgetc(file)) != EOF && c != ';') {
			buff[cptChar] = c;
			cptChar++;
		}
		buff[cptChar+1] = '\0';
		int n = atoi(buff);/*n est la taille du tableau*/
		cptChar = 0;

		plateau = (point**)malloc(n*sizeof(point*));
		for (i=0 ; i<n ; i++) {
			plateau[i] = (point*)malloc(n*sizeof(point));
		}

		for (i=0; i<n; i++) {
			for (j=0 ; j<n ; j++) {
				c = fgetc(file);

				if (c=='1'){  /*si le char='1' alors il y a une point*/
					plateau[i][j].flag=1;
					buff[0] = fgetc(file);

					while (buff[0]!=';'){ /*on regarde alors si il fait partie d'une ligne, ligne[] est un tableau de 2char, on teste le 2ème pour connaitre la ligne*/
						if (buff[0]=='0'){
							add_type_ligne(lh,&plateau[i][j].liste_type);
						}
						if (buff[0]=='1'){
							add_type_ligne(lv,&plateau[i][j].liste_type);
						}
						if (buff[0]=='2'){
							add_type_ligne(d1,&plateau[i][j].liste_type);
						}
						if (buff[0]=='3'){
							add_type_ligne(d2,&plateau[i][j].liste_type);
						}

						buff[0] = fgetc(file);
					}
				} else {/*si le char='0' la case est vide*/
					plateau[i][j].flag=0;
					plateau[i][j].liste_type = NULL;
					c = fgetc(file);/*on avance le curseur de 1, avec un buffer dont on se sert plus: taille*/
				}
			}
    	}

    	fclose(file);
    	return plateau;
	} else {
		return NULL;
	}
}

int is_over(point**plateau, int n) {
	return list_coups_valides(plateau,n)==NULL;
}

/*
int main() {
	int n = 12;
	couple c;
	list_ligne listeCoups = (list_ligne)empty();
	int nombreLignes;
	ligne l;
	c.x = 0;
	c.y = 0;
	l.createur = c;
	l.type = LH;
	l.tab_pts[0] = c;
	l.tab_pts[1] = c;
	l.tab_pts[2] = c;
	l.tab_pts[3] = c;
	l.tab_pts[4] = c;

	point **plateau = random_plateau(n);
	print_game(plateau, n);
	c = saisir_coup(plateau, n);
	play_coup(plateau, c, l, &listeCoups, &nombreLignes);
	print_game(plateau, n);
}
*/
