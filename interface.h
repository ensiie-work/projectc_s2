/**
* \file interface.h
* \brief Communication avec l'utilisateur
*
* Fonctions de l'interface entre l'utilisateur et le jeu : afficher le jeu,
* demander un coup à l'utilisateur, définir la taille, afficher l'aide...
*/

#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdio.h>
#include "plateau.h"
#include <gtk/gtk.h>
#include "gtk-gui/gui.h"
#include "gtk-gui/canvas.h"
#include "gtk-gui/gui-top.h"

/**
* \brief différentes actions que peut effectuer le joueur
*/
typedef enum action {JOUER_COUP, ANNULER, REJOUER, HELP, SAVE} action;

/**
* \brief affiche le plateau de jeu, les coups joués, les alignements et les coups possibles
* \param plateau le plateau de jeu
* \param n la taille du plateau
*/
void print_game(point **plateau, int n);

/**
* \brief permet au joueur de saisir un coup, affiche les coups possibles et/ou l'aide
* \param coups_possibles la liste des coups possibles précalculée
* \param coupChoisi le coup qui aura éventuellement été choisi
* \param ligneChoisie la ligne qui aura éventuellement été choisie
* \return l'action qui a été faite
*/
enum action saisir_coup(list_couple coups_possibles, couple* coupChoisi, ligne* ligneChoisie, point** plateau, int n);

/**
* \brief permet au joueur de saisir une ligne, affiche les lignes possibles et/ou l'aide
* \param lignes_possibles la liste des lignes possibles précalculée
* \return la position du pion correspondant au coup saisi
*/
ligne saisir_ligne(list_ligne lignes_possibles);

/**
* \brief permet au joueur de choisir une action (saisir coup, revenir en arrière, etc.)
* \return l'action choisie
*/
action choisir_action();

/**
* \brief permet d'ouvrir l'interface graphique GTK
* \param plateau le plateau de jeu
* \param n la taille du plateau
* \return l'objet interface graphique
*/
void open_gui(point **plateau, int n);

/**
* \brief affiche le score
* \param nombreLignes le nombre de lignes
*/
void afficher_score(int *nombreLignes);

/**
* \brief permet au joueur de définir la taille de son plateau (si ce dernier n'est pas importé par fichier)
* \param n la variable de taille de tableau
* \return 1 si la taille est valide, 0 sinon
* \attention modifie n
*/
int definir_TaillePlateau(int *n);

/**
* \brief permet au joueur de définir le nom du fichier de la sauvegarde de son plateau
* \param le buffer qui va contenir le nom du fichier
*/
void definir_nomFichier(char *buff);

/**
* \brief affiche une aide sur l'utilisation de l'exécutable
*/
void help();

/**
* \brief affiche une erreur si un fichier de plateau n'a pas pu être lu
*/
void file_exception();

/**
* \brief affiche un message précisant que la commande demandée est inconnue
*/
void commande_inconnue();

#endif
