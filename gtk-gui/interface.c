#include "interface.h"
#include "listes.h"

Tgui *init() {
	return NULL;
}

void print_game(point **plateau, int n) {
	int 	i;
	int 	j;
	char 	*num_h;
	char 	*num_v;
	int 	flagLH = 0;
	int		flagLV = 0;

	num_h = (char *) malloc (3*n*sizeof(char)+1);
	num_v = (char *) malloc (2*n*sizeof(char)+1);

	printf("  ");
	for (i=0 ; i<n ; i++) {
		printf(" ");
		if (n<10)
			printf("0");
		else
		  printf("%i",(i+1)/10);
	}
	printf("\n");
	printf("  ");
	for (i=0 ; i<n ; i++) {
		printf(" ");
		printf("%i",(i+1)%10);
	}
	printf("\n");
	for (i=0 ; i<n ; i++) {
		printf("%2i ",i+1);
		for (j=0 ; j<n ; j++) {
			if (plateau[i][j].flag) {
				printf("O");

				/* affichage des lignes horizontales */
				if (flagLH==0 && contains(LH,plateau[i][j].liste_type))
					flagLH = 1;
				if (flagLH<5 && flagLH>0 && contains(LH,plateau[i][j].liste_type)) {
					printf("-");
					flagLH++;
				}
				else {
					flagLH = 0;
					printf(" ");
				}
			}
			else printf("  ");
		}
		printf("\n");
		printf("   ");
		/* affichage des lignes verticales et diagonales */
		for (j=0 ; j<n ; j++) {
			if (contains(LV,plateau[i][j].liste_type) && i+1<n && contains(LV,plateau[i+1][j].liste_type)) {
				printf("|");
			}
			else printf(" ");
			if (contains(D2,plateau[i][j].liste_type) && i+1<n && j+1<n && contains(D2,plateau[i+1][j+1].liste_type)) {
				if (contains(D1,plateau[i][j+1].liste_type) && contains(D1,plateau[i+1][j].liste_type))
					printf("X");
				else printf("\\");
			}
			else if (j+1<n && contains(D1,plateau[i][j+1].liste_type) && i+1<n && contains(D1,plateau[i+1][j].liste_type)) {
				printf("/");
			}
			else printf(" ");

		}
		printf("\n");
	}
}

/*action {JOUER_COUP, ANNULER, REJOUER, HELP, SAVE} action*/
action choisir_action(){
  char c[100];
  int m;
  int flag=0;
  while (flag==0){
    printf ("Saisissez 1 pour jouer le coup, 2 pour annuler, 3 pour rejouer, 4 pour afficher l'aide, 5 pour sauvegarder le plateau\n");
    scanf("%1s",c);
    m=strtol(c,NULL,10);
    if ((m>=1)&&(m<=5)) flag=1;
  }
  if (m==1) return JOUER_COUP;
  if (m==2) return ANNULER;
  if (m==3) return REJOUER;
  if (m==4) return HELP;
  if (m==5) return SAVE;
  else return HELP;
}

void open_gui() {
	//TODO
	return NULL;
}

void interface_addLine(Tgui* gui, ligne line) {
	//TODO
}

void interface_supLastLine(Tgui* gui) {
	//TODO
}

enum action saisir_coup(list_couple coups_possibles, couple* coupChoisi, ligne* ligneChoisie){
	enum action action = choisir_action();

	if(action == JOUER_COUP) {
		couple coord;
		char c;

		printf("Voulez-vous afficher la liste des coups possibles ? (Y/N)\n");
		scanf("%1s",&c);
		if (c=='Y' || c=='y'){
			printf("Les coups possibles sont :\n");
			while (coups_possibles != NULL){
				printf("%i,%i ",coups_possibles->val.x+1, coups_possibles->val.y+1);
				coups_possibles=coups_possibles->next;
			}
		}

		printf("\nAfficher l'aide? (Y/N)\n");
		scanf("%1s",&c);
		if (c=='Y' || c=='y') help();
		printf("Saisir le coup :\n");
		scanf("%i",&coord.x);
		scanf("%i",&coord.y);
		coord.x -= 1;
		coord.y -= 1;
		*coupChoisi = coord;
	}

	return action;
}

ligne saisir_ligne(list_ligne lignes_possibles) {
	char const* nom_type_ligne[] = {"L horizontale", "L verticale", "L diagonale 1", "L diagonale 2"};
	char ch;
	ligne l;
	int k;
	list_ligne start = lignes_possibles;
	couple err;

	err.x = -1;
	err.y = -1;

	int type_ligne_int;

	printf("Les lignes possibles sont :\n");
	while (lignes_possibles != NULL){
		printf("%s\n", nom_type_ligne[lignes_possibles->val.type]);
		lignes_possibles = lignes_possibles->next;
	}

	printf("Saisir le type de ligne souhaité:\n");
	printf("\t1: horizontale\n\t2: verticale\n\t3: diagonale /\n\t4: diagonale \\\n");
	scanf("%1s",&ch);
	type_ligne_int = strtol(&ch,NULL,10);
	while (type_ligne_int<=0 || type_ligne_int>4) {
		printf("Veuillez entrer une ligne valide :\n");
		printf("\t1: horizontale\n\t2: verticale\n\t3: diagonale /\n\t4: diagonale \\\n");
		fflush(stdin);
		scanf("%1s",&ch);
		type_ligne_int = strtol(&ch,NULL,10);
	}
	type_ligne_int--;
	l.type = (type_ligne)type_ligne_int;

	/* IMPORTANT */
	lignes_possibles = start;
	int ligne_found = 0;
	while (lignes_possibles != NULL && !ligne_found){
		if(lignes_possibles->val.type == l.type) {
			l.createur = (lignes_possibles->val).createur;
			for (k = 0 ; k<5 ; k=k+1) {
				l.tab_pts[k] = (lignes_possibles->val).tab_pts[k];
			}
			l.type = (lignes_possibles->val).type;
			ligne_found = 1;
		}
		lignes_possibles = lignes_possibles->next;
	}
	/* si le type de ligne souhaité n'est pas disponible */
	if (ligne_found==0) {
		l.createur = err;
		return l;
	}
	return l;
}

void definir_nomFichier(char *buff) {
	printf("Saisissez le nom du fichier à exporter\n");
	scanf("%s",buff);
}

void file_exception() {
	printf("Le chargement/sauvegarde de votre plateau a échoué. Vérifiez votre fichier ou votre nom de fichier.\n");
}

void afficher_score(int *nombreLignes){
	printf("score :%i\n",*nombreLignes);

}

int definir_TaillePlateau(int *n){
  char c[100];
  int m;
  printf("Saisissez la taille du plateau :\n");
  scanf("%s",c);
  m=strtol(c,NULL,10);
  if (m<=0)return 0;
  else{
    *n=m;
    return 1;
  }

}

void help(){
  printf("Pour lancer l'exécutable, vous pouvez soit mettre aucun argument, soit donner un argument qui est le nom du fichier source (contenant le plateau éventuellement sauvegardé)\n");

}

void commande_inconnue(){
  printf("La commande saisie est inconnue\n");
}
