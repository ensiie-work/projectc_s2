/**
* \file plateau.h
* \brief Définition des types et des fonctions de jeu pour le plateau
*
* Déclarations des types utilisés, fonctions de création du plateau et de jeu
* (liste des coups valides, jouer un coup...)
*/

#ifndef PLATEAU_H
#define PLATEAU_H

#include <stdlib.h>

/**
* \brief couple : les coordonées d'un point
*/
typedef struct couple {
	int x; /**< la position sur l'axe x */
	int y; /**< la position sur l'axe y */
} couple;

/**
* \brief différents types de lignes : horizontale, verticale, diagonale
*/
typedef enum type_ligne {LH, LV, D1, D2} type_ligne;

/**
* \brief liste de type_ligne
* ex: [LH,LV,LH,...]
*/
typedef struct maillon_type_ligne * list_type_ligne;

/**
* \brief maillon d'une liste de type_ligne
*/
struct maillon_type_ligne {
	type_ligne type; /**< le type de ligne du maillon actuel : LH,LV...*/
	list_type_ligne next; /**< le pointeur vers le maillon suivant*/
};

/**
* \brief type d'un point
*/
typedef struct point {
	int 	flag; /**< si flag = 1, on considère qu'un point est présent */
	list_type_ligne liste_type; /**< la liste de type_ligne des lignes auxquelles
								le point appartient*/
} point;

/**
* \brief définition du type ligne : un tableau de 5
* coordonées (dans l'ordre : [0] est le pt le plus à gauche
* ou en haut pour une ligne verticale)
*/
typedef struct ligne {
	couple tab_pts[5]; /**< les coordonées des 5 points de la ligne*/
	type_ligne type; /**< le type de la ligne*/
	couple createur; /**< le point qui a créé la ligne*/
} ligne;

/**
* \brief liste de ligne
*/
typedef struct maillon_ligne * list_ligne;

/**
* \brief maillon d'une liste de ligne
*/
struct maillon_ligne {
	ligne val; /**< la ligne du maillon*/
	list_ligne next; /**< le pointeur vers la ligne suivante*/
};

/**
* \brief liste de couple
*/
typedef struct maillon_couple * list_couple;

/**
* \brief maillon d'une liste de couple
*/
struct maillon_couple {
	couple val; /**< le couple du maillon*/
	list_couple next; /**< le pointeur vers le couple suivant*/
};


/**
* \brief charge un plateau à partir d'un fichier
* \param path le chemin vers le fichier voulu
* \return le plateau de jeu correspondant au fichier
* \attention renvoie NULL si le fichier n'est pas valide
*/
point**file_to_plateau(char *path);

/**
* \brief charge la taille du plateau à partir du fichier
* \param path le chemin vers le fichier voulu
* \return la taille du plateau de jeu correspondant au fichier
* \attention renvoie NULL si le fichier n'est pas valide
*/
int file_plateau_size(char *path);

/**
* \brief charge un plateau aléatoire
* \param n la taille du plateau
* \return le plateau aléatoire
* \attention \b n doit être positif
*/
point**random_plateau(int n);

/**
* \brief écrit le plateau de jeu dans un fichier
* \param plateau le plateau de jeu
* \param dest le chemin du fichier
* \param n la taille du plateau
* \return 0 si l'écriture a réussi, -1 sinon
* \attention le chemin doit être valide
*/
int plateau_to_file(point**plateau, char *dest, int n);

/**
* \brief donne une liste des coups valides
* \param plateau le plateau de jeu
* \param n la taille du plateau
* \return une liste de couples correspondants aux coups possibles
*/
list_couple list_coups_valides(point**plateau, int n);

/**
* \brief donne une liste des lignes possibles pour une coordonée donnée
* \param plateau le plateau de jeu
* \param n la taille du plateau
* \param c le couple qui va créer les différentes lignes
* \return la liste des lignes possibles pour le point \b c
*/
list_ligne list_lignes_possibles(point **plateau, int n, couple c);

/**
* \brief joue un coup : place le pion
* \param  plateau le plateau de jeu
* \param c la position correspondant au coup
* \param l la ligne que crée le coup
* \param listeCoups la liste de ligne qui correspond à tous les coups déjà joués
* \param nombreLignes le nombre de lignes dans listeCoups
*/
void play_coup(point **plateau, couple c, ligne l, list_ligne *listeCoups, int *nombreLignes);

/**
* \brief vérifie que la partie est terminée
* \param plateau le plateau de jeu
* \param n la taille du plateau
* \return 1 si le jeu est terminé, 0 sinon
*/
int is_over(point**plateau, int n);


#endif
