/**
* \file listes.h
* \author Charles Anteunis
* \brief Implémentation des listes
*
* Fonctions de création et de manipulation des différentes listes chainées
*/

#ifndef LISTES_H
#define LISTES_H

#include "plateau.h"

/**
* \brief vérifie qu'une liste est vide ou non
* \param l la liste à vérifier
* \return true si la liste est vide, false sinon
*/
int is_empty(void * l);

/**
* \brief renvoie une liste vide. Penser à cast en le bon type de ligne
* par exemple : list_ligne l = (list_ligne) empty();
* \return une liste vide du type du cast de l'appel
*/
void *empty();

/**
* \brief ajoute une ligne en tête d'une liste de ligne
* \param li la ligne que l'on souhaite ajouter
* \param l la liste à modifier
*/
void add_ligne(ligne li, list_ligne *l);

/**
* \brief renvoie la tête d'une liste de ligne en modifiant cette liste
* \param l la liste à modifier
* \return la ligne en tête de la liste
*/
ligne pop_ligne(list_ligne *l);

/**
* \brief ajoute une ligne en tête d'une liste de type_ligne
* \param tl le type_ligne que l'on souhaite ajouter
* \param l la liste à modifier
*/
void add_type_ligne(type_ligne tl, list_type_ligne *l);

/**
* \brief renvoie la tête d'une liste de type_ligne en modifiant cette liste
* \param l la liste à modifier
* \return le type_ligne en tête de la liste
*/
type_ligne pop_type_ligne(list_type_ligne *l);

/**
* \brief ajoute une ligne en tête d'une liste de couple
* \param c le couple que l'on souhaite ajouter
* \param l la liste à modifier
*/
void add_couple(couple c, list_couple *l);

/**
* \brief renvoie la tête d'une liste de couple en modifiant cette liste
* \param l la liste à modifier
* \return le couple en tête de la liste
*/
couple pop_couple(list_couple *l);

/**
* \brief vérifie si une liste contient un type_ligne particulier
* \param t le type_ligne
* \param l la liste dans laquelle on cherche \b t
* \return 1 si l contient t, 0 sinon
*/
int contains(type_ligne t, list_type_ligne l);

/**
* \brief vérifie si une liste de couples contient un couple (point) particulier
* \param c le couple
* \param lc la liste dans laquelle on cherche \b c
* \return 1 si lc contient c, 0 sinon
*/
int contains_couple(couple c, list_couple lc);

/**
* \brief vérifie si une liste de lignes contient une ligne particulière
* \param l la ligne
* \param ll la liste dans laquelle on cherche \b l
* \return 1 si ll contient l, 0 sinon
*/
int contains_ligne(ligne l, list_ligne ll);

/**
* \brief retourne la taille d'une liste de couple
* \param l la liste de couples
* \return l'entier correspondant à sa taille
*/
int len_couple(list_couple l);

#endif
