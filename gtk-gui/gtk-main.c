#include <gtk/gtk.h>

#include "gui.h"
#include "canvas.h"
#include "historique.h"
#include "plateau.h"
#include "listes.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


static Tint2 points[]    = { { 4,4}, {5,5} };
static Tsegm lines[]     = { { { 6,6}, {6,11} },  { { 6,6}, {11,6} },
                             { { 6,6}, {10,8} }, { { 2,4}, { 6,6} },
                             { { 6,6}, {10,4} }, { { 2,8}, { 6,6} },
};
static Tsegm linesHelp[] = { { { 1,1}, { 1,5} }, { { 1,2}, { 1, 6} },
                             { { 1,3}, { 1,7} }, { { 1,4}, { 1, 8} },
                             { { 1,5}, { 1,9} }, { { 1,6}, { 1,10} },
                             { { 1,1}, { 5,5} }, { { 1,1}, { 5, 1} },
};

int main()
{
    Taction action;
    Tsegm   line;
    Tgui* gui = gui_open(300,20);

	point 			**plateau;
	int				n 	= 0;
	int				nombreLignes = 0;
	int 			coupPossible = 1;
	list_ligne 		listeCoups = (list_ligne)empty();
	list_ligne		listeCoupsAnnules = (list_ligne) empty();
	couple			coupChoisi;
	couple 			realCoupChoisi;
	ligne			ligneChoisie;
	list_ligne		lignesPossibles =  (list_ligne)empty();
	list_couple		coupsPossibles =  (list_couple)empty();
	list_ligne		tmplignesPossibles =  (list_ligne)empty();
	list_couple		tmpcoupsPossibles =  (list_couple)empty();
	char 			buff[1024];

	Tint2 *pointsPlateau;
	Tsegm *lignesHelp;
	Tsegm lastLine;

    int fini=0;
	int flag = 0;
  	int i=0, j=0;
	int count = 0;
	n = 6;
	int nListe = 0;
	int nbPoints = 0;
	Tint2 tmp;
	plateau = random_plateau(n);

	for (i=0 ; i<n ; i++) {
		for (j=0 ; j<n ; j++) {
			if (plateau[i][j].flag) nbPoints++;
		}
	}
	pointsPlateau = (Tint2*)malloc(nbPoints*sizeof(Tint2));
	for (i=0 ; i<n ; i++) {
		for (j = 0; j < n; j++) {
			if (plateau[i][j].flag) {
				tmp.x = i;
				tmp.y = j;
				pointsPlateau[count] = tmp;
				count++;
			}
		}
	}
	gui_addPoints(gui,pointsPlateau,nbPoints);
	gui_free(pointsPlateau);

     while ( ! fini ) {
		 flag = 0;
        gui_redraw(gui);
        switch ( gui_getAction(gui,&line) ) {
          case GUI_ACT_Segment:
              if ( abs(line.p1.x-line.p2.x)>9||
                   abs(line.p1.y-line.p2.y)>9 )
                  gui_error(gui, "le segment (%d,%d) --> (%d,%d) est invalide",
                        line.p1.x,line.p1.x, line.p2.y,line.p2.y);
              else
                  gui_addLines(gui,&line,1);
              break;
          case GUI_ACT_Undo:
              gui_supLastLine(gui);
              break;
	  case GUI_ACT_Redo:
	      gui_addLines(gui,&line,1);
	      break;

          case GUI_ACT_Help:
		  	coupsPossibles = list_coups_valides(plateau,n);
			if (coupsPossibles==NULL) {
				fini = 1;
				break;
			}
			tmpcoupsPossibles = coupsPossibles;
			nbPoints = len_couple(coupsPossibles);
			lignesHelp = (Tsegm*)malloc(nbPoints*8*sizeof(Tsegm));
			count = 0;

			/*pour chaque couple possible on va calculer les lignes possibles*/
			while (coupsPossibles!=NULL) {
				coupChoisi.x=coupsPossibles->val.x;
				coupChoisi.y=coupsPossibles->val.y;
				lignesPossibles = list_lignes_possibles(plateau,n,coupChoisi);
				/*on parcourt les lignes possibles pour les ajouter au tableau*/
				while (lignesPossibles!=NULL) {
					tmp.x = lignesPossibles->val.tab_pts[0].x;
					tmp.y = lignesPossibles->val.tab_pts[0].y;
					lignesHelp[count].p1 = tmp;
					tmp.x = lignesPossibles->val.tab_pts[4].x;
					tmp.y = lignesPossibles->val.tab_pts[4].y;
					lignesHelp[count].p2 = tmp;
					count++;
					lignesPossibles = lignesPossibles->next;
				}
				coupsPossibles = coupsPossibles->next;
			}

              if ( gui_getSegOfSet(gui,lignesHelp, count, &line)==GUI_ACT_Quit)
                  goto end;
			  lastLine = line;
			  while (tmpcoupsPossibles!=NULL) {
  				coupChoisi.x=tmpcoupsPossibles->val.x;
  				coupChoisi.y=tmpcoupsPossibles->val.y;
  				lignesPossibles = list_lignes_possibles(plateau,n,coupChoisi);
  				/*on parcourt les lignes possibles pour trouver la ligne correspondant
				à celle de l'utilisateur*/
  				while (lignesPossibles!=NULL) {
  					if (lignesPossibles->val.tab_pts[0].x == lastLine.p1.x &&
  						lignesPossibles->val.tab_pts[0].y == lastLine.p1.y &&
  						lignesPossibles->val.tab_pts[4].x == lastLine.p2.x &&
  						lignesPossibles->val.tab_pts[4].y == lastLine.p2.y) {
							ligneChoisie = lignesPossibles->val;
							flag = 1;
							printf("ligne trouvée\n");
							printf("user: %i, %i, %i, %i\n",lastLine.p1.x,lastLine.p1.y,lastLine.p2.x,lastLine.p2.y);
							printf("\n");
							break;
						}
  					lignesPossibles = lignesPossibles->next;
  				}
				if(flag) break;
  				tmpcoupsPossibles = tmpcoupsPossibles->next;
  			}
			if(!flag) gui_error(gui, "le segment (%d,%d) --> (%d,%d) est invalide",
				  line.p1.x,line.p1.x, line.p2.y,line.p2.y);
			realCoupChoisi = ligneChoisie.createur;
			  play_coup(plateau,realCoupChoisi,ligneChoisie,&listeCoups,&nombreLignes);

              gui_addLines(gui,&line,1);
              break;
          case GUI_ACT_Quit:
              fini = 1;
              break;
          default:
              fprintf(stderr,"unexpected action\n");
        }
    }

end:
    gui_close(gui);

    return 0;
}
