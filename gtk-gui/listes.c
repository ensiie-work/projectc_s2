#include "listes.h"

int is_empty(void * l) {
	return l==NULL;
}

void *empty() {
	return NULL;
}

void add_ligne(ligne li, list_ligne *l) {
	list_ligne	tmp;

	tmp = (list_ligne) malloc(sizeof(struct maillon_ligne));
	tmp->val = li;
	tmp->next = *l;
	*l = tmp;
}

ligne pop_ligne(list_ligne *l) {
	ligne		res;
	list_ligne 	tmp;

	res = (*l)->val;
	tmp = *l;
	*l = (*l)->next;
	free(tmp);
	return res;
}

void add_type_ligne(type_ligne tl, list_type_ligne *l) {
	list_type_ligne	tmp;

	tmp = (list_type_ligne) malloc(sizeof(struct maillon_type_ligne));
	tmp->type = tl;
	tmp->next = *l;
	*l = tmp;
}

type_ligne pop_type_ligne(list_type_ligne *l) {
	type_ligne		res;
	list_type_ligne tmp;

	res = (*l)->type;
	tmp = *l;
	*l = (*l)->next;
	free(tmp);
	return res;
}

void add_couple(couple c, list_couple *l) {
	list_couple tmp;

	tmp = (list_couple) malloc (sizeof(struct maillon_couple));
	tmp->val = c;
	tmp->next = *l;
	*l = tmp;
}

couple pop_couple(list_couple *l) {
	couple res;
	list_couple tmp;

	res = (*l)->val;
	tmp = *l;
	*l = (*l)->next;
	free(tmp);
	return res;
}

int contains (type_ligne t, list_type_ligne l) {
	while (l!=NULL) {
		if (l->type==t) return 1;
		l=l->next;
	}
	return 0;
}

int contains_couple (couple c, list_couple lc) {
	while (lc!=NULL) {
		if (lc->val.x==c.x && lc->val.y==c.y) return 1;
		lc=lc->next;
	}
	return 0;
}

int contains_ligne (ligne l, list_ligne ll) {
	int ligne_found = 1;
	int i;
	/* -1 en coordonee signifie une erreur */
	if (l.createur.x==-1) return 0;
	while(ll!=NULL) {
		ligne_found = 1;
		for(i = 0; i < 5; i++) {
			if (l.tab_pts[i].x != ll->val.tab_pts[i].x || l.tab_pts[i].y != ll->val.tab_pts[i].y) ligne_found = 0;
		}
		if(ligne_found == 1) return 1;
		ll=ll->next;
	}
	return 0;
}

int len_couple(list_couple l) {
	int i = 0;
	while (l!=NULL) {
		i++;
		l=l->next;
	}
	return i;
}
