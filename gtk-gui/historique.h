/**
* \file historique.h
* \brief Fonctions d'historique des coups
*
* Fonctions permettant d'annuler un coup ou de reoujouer le dernier coup joué
*/

#ifndef HISTORIQUE_H
#define HISTORIQUE_H

#include "plateau.h"

/**
* \brief annule le dernier coup joué
* \param plateau le plateau de jeu
* \param nombreLignes le nombre de lignes
* \param listeCoups la liste de coups
* \param listeCoupsAnnules la liste des coups annulés
* \attention modifie le plateau et la liste des coups + liste des coups annulés
*/
void annuler(point **plateau, list_ligne *listeCoups, list_ligne *listeCoupsAnnules, int *nombreLignes);

/**
* \brief rejoue le coup précédemment annulé
* \param plateau le plateau de jeu
* \param nombreLignes le nombre de lignes
* \param listeCoups la liste de coups
* \param listeCoupsAnnules la liste des coups annulés
* \param retourne la ligne du coup rejoué
* \attention modifie le plateau et la liste des coups + liste des coups annulés
* \attention \b listeCoupsAnnules doit être initialisée et non NULL
*/
ligne rejouer_coup(point **plateau, list_ligne *listeCoups, list_ligne *listeCoupsAnnules, int *nombreLignes);

#endif
