#include "historique.h"
#include "listes.h"

void annuler(point **plateau, list_ligne *listeCoups, list_ligne *listeCoupsAnnules, int *nombreLignes) {
    /* il faut aller chercher le dernier coup dans la liste de coups effectué */

	/* vérification pour ne pas pop sur du vide */
	if (*nombreLignes == 0) return;
	ligne l = pop_ligne(listeCoups);
    add_ligne(l,listeCoupsAnnules);
	int i;

    /* enlever le point de la pile de points */
    plateau[l.createur.x][l.createur.y].flag = 0;
    for(i=0; i < 5; i++)  {
        pop_type_ligne(&plateau[l.tab_pts[i].x][l.tab_pts[i].y].liste_type);
    }

    (*nombreLignes)--;
}

ligne rejouer_coup(point **plateau, list_ligne *listeCoups, list_ligne *listeCoupsAnnules, int *nombreLignes) {

	ligne l = pop_ligne(listeCoupsAnnules);
    add_ligne(l,listeCoups);
	int i;

    plateau[l.createur.x][l.createur.y].flag = 1;
    for(i=0; i < 5; i++) {
        add_type_ligne(l.type,&plateau[l.tab_pts[i].x][l.tab_pts[i].y].liste_type);
    }

    (*nombreLignes)++;

    return l;
}
